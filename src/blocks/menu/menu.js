$(document).on('click', '.js-anchor', function (event) {
  event.preventDefault();
  $('.popup').fadeOut('slow');
  $('.header').removeClass('header--popup');

  $('html, body').animate({
    scrollTop: $($.attr(this, 'href')).offset().top - 102
  }, 500);
});
