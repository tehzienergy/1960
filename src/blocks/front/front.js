$.preloadImages = function() {
  for (var i = 0; i < arguments.length; i++) {
    $("<img />").attr("src", arguments[i]);
  }
}

$.preloadImages("img/1.svg", "img/9.svg", "img/6.svg", "img/0.svg", "img/sign.png", "img/keeper.png", "img/player.png", "img/photo.png", "img/winner.png", "img/cup.png", "img/radio.png");

jQuery(document).ready(function() {

  if ($(window).width() > 992) {

    $('.front__cover').fadeOut();

    window.sr = ScrollReveal();

    sr.reveal('.header__logo', {
      origin: 'top',
      duration: 1000,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '50%',
      scale: 1,
      delay: 0,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: 1,
      container: 'body'
    });

    sr.reveal('.header__menu', {
      origin: 'top',
      duration: 700,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '50%',
      scale: 1,
      delay: 700,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: 1,
      container: 'body'
    });

    sr.reveal('.header__link', {
      origin: 'top',
      duration: 700,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '50%',
      scale: 1,
      delay: 700,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: 1,
      container: 'body'
    });

    sr.reveal('.front__item--bg-dark', {
      origin: 'right',
      duration: 1000,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '100%',
      scale: 1,
      delay: 0,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: .1
    });

    sr.reveal('.front__item--bg-light', {
      origin: 'right',
      duration: 3000,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '100%',
      scale: 1,
      delay: 1000,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: .1
    });

    sr.reveal('.front__dscr', {
      origin: 'top',
      duration: 700,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '30%',
      scale: 1,
      delay: 700,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: 1
    });

    sr.reveal('.front__item--1', {
      origin: 'bottom',
      duration: 1500,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '20%',
      scale: 1,
      delay: 700,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: .1
    });

    sr.reveal('.front__item--9', {
      origin: 'bottom',
      duration: 1500,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '20%',
      scale: 1,
      delay: 1000,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: .1
    });

    sr.reveal('.front__item--6', {
      origin: 'top',
      duration: 1500,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '20%',
      scale: 1,
      delay: 1300,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: .1
    });

    sr.reveal('.front__item--0', {
      origin: 'top',
      duration: 1500,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '20%',
      scale: 1,
      delay: 1600,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: .1
    });

    sr.reveal('.front__item--winner', {
      origin: 'right',
      duration: 1500,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '20%',
      scale: 1,
      delay: 1300,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: .1
    });

    sr.reveal('.front__item--cup', {
      origin: 'right',
      duration: 1500,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '0',
      scale: 1,
      delay: 3000,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: .1
    });

    sr.reveal('.front__item--radio', {
      origin: 'right',
      duration: 1500,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '20%',
      scale: 1,
      delay: 1300,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: .1
    });

    sr.reveal('.front__item--sign', {
      origin: 'right',
      duration: 1500,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '20%',
      scale: 1,
      delay: 1600,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: .1
    });

    sr.reveal('.front__item--keeper', {
      origin: 'right',
      duration: 1500,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '20%',
      scale: 1,
      delay: 1900,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: .1
    });

    sr.reveal('.front__item--photo', {
      origin: 'right',
      duration: 1500,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '20%',
      scale: 1,
      delay: 2200,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: .1
    });

    sr.reveal('.front__item--player', {
      origin: 'right',
      duration: 1500,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '20%',
      scale: 1,
      delay: 2500,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: .1
    });

    sr.reveal('.front__link', {
      origin: 'top',
      duration: 1500,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '30%',
      scale: 1,
      delay: 1300,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: .1
    });

    sr.reveal('.front__btn', {
      origin: 'top',
      duration: 700,
      easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
      distance: '30%',
      scale: 1,
      delay: 1000,
      mobile: false,
      reset: false,
      opacity: 0,
      viewFactor: 1
    });

  }

});
