if ($(window).width() > 992) {

  $('.match__minute').click(function (e) {
    e.preventDefault();
    $('.match__minute').removeClass('match__minute--active')
    $(this).addClass('match__minute--active');
    $('.match__media-item').removeClass('match__media-item--active');
    $('.match__media-item').eq($(this).index()).addClass('match__media-item--active');
    $('.match__media-video').prop('muted', true);
    $('.match__media-item--active').find('.match__media-video').prop('muted', false);
  })

};

if ($(window).width() < 993) {

  $('.match__media').slick({
    arrows: false,
    infinite: false,
    touch: false
  }).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    $('.match__dot').removeClass('match__dot--active');
    $('.match__dot').eq(nextSlide).addClass('match__dot--active');
    $('.match__timeline').slick('slickGoTo', nextSlide);
//    $('.match__media-video').prop('muted', true);
//    $('.match__media-item').eq(nextSlide).find('.match__media-video').prop('muted', false);
  });

  $('.match__timeline').slick({
    arrows: false,
    infinite: false,
    swipe: false,
    fade: true
  });

  $('.match__dot').click(function (e) {
    e.preventDefault();
    $('.match__dot').removeClass('match__dot--active');
    $(this).addClass('match__dot--active');
    var slideNumber = $(this).attr('href') - 1;
    $('.match__media').slick('slickGoTo', slideNumber);
    $('.match__timeline').slick('slickGoTo', slideNumber);
  });
}

if ($(window).width() < 1200) {
  $('.match__media-video').each(function () {
    $(this).get(0).play();
  })
}


if ($(window).width() >= 1200) {
  var trigger = $('.match__media').offset().top - $(window).height() / 2;
  
  $("body, html").bind("change click contextmenu dblclick mouseup pointerup reset submit touchend", function (e) {
    if ($(window).scrollTop() > trigger) {
      $('.match__media-item--first').removeClass('match__media-item--first');
      $('.match__media-item--active').find('.match__media-video').prop('muted', false);
    }
    else {
      $('.match__media-item--first').removeClass('match__media-item--first');
    }
  });

  $(window).on('scroll', function () {
    if (($(window).scrollTop() > trigger) && (!$('.match__media-item--active').hasClass('match__media-item--first'))) {
      $('.match__media-item--active').find('.match__media-video').prop('muted', false);
      setTimeout(function() {
        $('.match__media-item--active').find('.match__media-video').get(0).play();
      }, 500)
    }
  });
}
